---
id: d9qfop46ses4ekwnqovcuz1
title: Challenges
desc: ''
updated: 1658265306623
created: 1658235358114
---
- [back to index](root.md)
# Workbook 2.1
## Activity – Challenges
![Challenges](media/sailing.jpg "teamwork with challenges")
## 6. Select the top challenges you are likely to encounter in your environment as it relates to ML data, models, and code.

| Data                                                                                                                                                                     | Model                                                                                                                                                                                 | Code                                                                                                                                                                                                              |
|---------------------|-----------------------|----------------------------|
| ☐ Providing mechanisms by which training / testing / validation datasets, training scripts, models, experiments, and service wrappers can all be versioned appropriately | ☐ Managing changes to training / testing / validation datasets, training scripts, models, experiments, and service wrappers, and ensuring each is auditable across the full lifecycle | ☐ Knowing the risks of trying to use Jupyter notebooks in production                                                                                                                                              |
| ☐ Treating training / testing / validation datasets as managed assets under an MLOps workflow                                                                            | ☐ Enabling all ML frameworks to be used within the scope of MLOps, regardless of language or platform                                                                                 | ☐ Methods for wrapping trained models as deployable services in scenarios where the data scientists training the models might not be experienced software developers with a background in service-oriented design |
| ☐ Applying MLOps to very large-scale problems at petabyte scale and beyond                                                                                               | ☐ Enabling MLOps to support a broad range of target platforms, including but not limited to CPU, GPU, TPU, custom ASICs, and neuromorphic silicon                                     | ☒ Providing appropriate pipeline tools to manage MLOps workflows transparently as part of existing DevOps solutions                                                                                               |
| ☐ Access to key performance indicator business data for MLOps team members who need to evaluate model effectiveness                                                      | ☒ Ensuring efficient use of hardware in both training and operational scenarios                                                                                                       | ☐ Model abstraction                                                                                                                                                                                               |
|                                                                                                                                                                          | ☐ Testing ML assets appropriately                                                                                                                                                     | ☐ Maximize longevity of ML assets                                                                                                                                                                                 |
|                                                                                                                                                                          | ☐ Online ML                                                                                                                                                                           | ☐ Ability to quickly cut out a model or roll back immediately to an earlier version                                                                                                                               
- [next challenge 2.2 - DevOps to MLOps](DevOps_to_MLOps.md)
- [next challenge 2.3 - Frameworks and Tooling](Enhancements.md)
- [next challenge 2.4 - Enhancements](Enhancements.md)
- [back to index](root.md)
