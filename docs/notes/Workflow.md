---
id: mzmzawqwzibs3dpp8z3pngf
title: Workflow
desc: ''
updated: 1658295653714
created: 1658235384297
---
- [back to index](root.md)
# 2.3 Activity – Workflow/Frameworks/Tooling
![Alt](media/AI-EYE.PNG "# Activity – Frameworks and Tooling")

## MLOps example workflow
<details>
  <summary>Click to show diagram</summary>

```mermaid

graph TD;
    extract_data-->evaluate_model;
    extract_data-->validate_model;
    extract_data-->generate_stats;
    extract_data-->transform_data;
    generate_stats-->validate_data;
    generate_stats-->generate_schema;
    generate_schema-->validate_data;
    generate_schema-->transform_data;
    generate_schema-->train_model;
    transform_data-->train_model;
    train_model-->evaluate_model;
    train_model-->validate_model;
    train_model-->serve_model;
    validate_model-->serve_model
```

Depending on the a use case, a training cycle can be restarted:

   - manually,
   - on schedule (daily, weekly. monthly),
   - once the new data is available,
   - once significant differences between training datasets and live data are spotted, or
   - once model performance drops below the baselines.

Each time the pipeline performs the following sequence of steps.
- **Data ingestion**. Any ML pipeline starts with data ingestion — in other words, acquiring new data from external repositories or feature stores, where data is saved as reusable “features”, designed for specific business cases. This step splits data into separate training and validation sets or combines different data streams into one, all-inclusive dataset.

- **Data validation**. The goal of this step is to make sure that the ingested data meets all requirements. If anomalies are spotted, the pipeline can be automatically stopped until data engineers fix the problem. It also informs if your data changes over time, highlighting differences between training sets and live data your model uses in production.

- **Data preparation**. Here, raw data is cleaned and quality checked output with a format so your model can use. At this step data scientists may intervene to combine raw data with domain knowledge and build new features, this feature engineering will be saved off to a Feature Store so that everyone is using identical features and can track the lineage of the data.

- **Model training**. At last, we come to the core of the entire pipeline. In the simplest scenario, the model is trained against freshly ingested and processed data or features. But you can launch several training runs in parallel or in sequence to identify the best parameters for a production model as well.

- **Model validation**. This when we test the final model performance across the dataset it has never seen before to confirm its readiness for deployment.

- **Data versioning**. Data versioning is the practice of saving data artifacts similar to code versions in software development.

</details>
## 11. Which end-to-end MLOps frameworks are you using?

- [ ] Not currently using an end-to-end MLOps solution

- [ ] Apache Airflow

- [ ] Kubeflow

- [X] Amazon SageMaker

- [X] AWS Step Functions

- [ ] Jenkins

- [ ] Other: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

## 12. Would migrating to an end-to-end solution for MLOps be better for your organization than adjusting the existing ML process? Why or why not? 

| There are multiple different automation pipeline solutions in place through acquisition, philosophical difference. Seeing ways we could us AWS API’s effectively with any of the existing solutions. Perhaps a team could develop API’s to be used by any of the solutions for Model Training and development. Current evaluation underway to utilize Amazon SageMaker Pipelines. |
|------------------------------------------------------------------------|

## 13. Action plan:

| Action / outcome description | Stakeholders | Considerations |
|------------------------------|--------------|----------------|
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |

## 14. Questions for the instructor:

|     |
|-----|

- [prev challenges 2.1 - Challenges](Challenges.md)
- [prev Challenges 2.2 - DevOps to MLOps](DevOps_to_MLOps.md)
- [next challenges 2.4 - Enhancements](Enhancements.md)
- [back to index](root.md)