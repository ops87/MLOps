---
id: k6079ljyc2pvryv3txbzwdp
title: Business_Requirements
desc: ''
updated: 1658265311822
created: 1658235641298
---
- [back to index](root.md)

# Workbook 4.1
## Activity – Business Objectives
![Business Objectives 4.1](media/vision.png "Business Objectives")



## 31. For each type of inference you use, what are the most important business requirements?

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<thead>
<tr class="header">
<th><p>All of our current solutions and image recognition solutions.
Accuracy is the most important model measurement. Application Stability
and version control are critical for the mobile applications and model
deployment.</p>
<p>We just started doing batch. Meeting batch processing deadlines is
critical for our new cache-based design.</p></th>
</tr>
</thead>
<tbody>
</tbody>
</table>


## 32. Are those business requirements measured? What baseline values or metrics are available?

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<thead>
<tr class="header">
<th><p>This is an area we are focused on for the next release. Speed to
market has been a primary driver but end-to-end quality is becoming more
critical. Some baselines exist based on currently released products.</p>
<p>DataOps and ML Engineering are looking at ways to simulate and
capture more and better data for model training. We are currently
looking at external data for purchase as well as partnering with area
universities.</p></th>
</tr>
</thead>
<tbody>
</tbody>
</table>

- [back to index](root.md)
- [next 4.2 - Model Monitoring](Model_Monitoring.md)
- [next 4.3 - Human in the Loop](Human_in_the_Loop.md)
- [next 4.4 - MLOps Monitoring](MLOps_Monitoring.md) 
