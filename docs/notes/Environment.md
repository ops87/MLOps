---
id: 6soes4j3altpadfwrgbzws4
title: Environment
desc: ''
updated: 1658267833524
created: 1658235462450
---
- [back to index](root.md)

## Workbook 3.1 Activity Solution Environments
![Workbook 3.1](media/cute-robot.jpg)

In this workbook activity, you will record characteristics of your ML
environment. This will help you think about the methods you currently
use for deployment. It will also help you consider options for MLOps
improvements as you study deployment alternatives.

## 18. How are you using ML in current and planned projects?

| We have multiple computer vision solutions in place with more ideas than we have time to deliver. There is more interest in integrating ML in internal systems for client churn analysis and product pricing analysis. |
|------------------------------------------------------------------------|

## 19. For what purpose are models packaged in deployment?

☐ Batch inference

☐ Online inference

☐ Both

☐ Not using either yet

## 20. At your organization, models are generally built using what ML framework (or these ML frameworks)?

| All of our solutions are MXNet and Gluon currently. |
|-----------------------------------------------------|

## 21. Models are built using what key ML algorithms?

| Most of our models use existing CNN’s with retraining. Some newer models are being developed based on existing CV models. Internal systems are looking at clustering algorithms and regression models for customer identification and predictions. |
|------------------------------------------------------------------------|

- [back to index](root.md)
- [next Environment 3.2 - Deployment_Solutions](Deployment_Solutions.md)
- [next Environment 3.3 - Deployment Strategies](Deployment_Strategies.md)