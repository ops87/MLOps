---
id: dvx80f636z6aeuki5kqpym4
title: Deployment_Solutions
desc: ''
updated: 1658267823550
created: 1658235597506
---
- [back to index](root.md)
# Workbook 3.2
## Activity – Deployment Solutions
![Workbook 3.2](media/deployAI.png)

## 22. What scaling strategies do you use in your ML process?

| We started on the cloud using AWS. We are familiar with Autoscaling/ Load Balancers and we use some serverless technology. Most of our deployments run on Mobile platforms but we are looking to use streaming and batch more as well as utilizing cache-based solutions. |
|------------------------------------------------------------------------|

## 23. Of the different ways to implement inference that were discussed, which are most similar to those you use in production?

- [X] Basic hosting inference

- [ ] Basic transform inference

- [ ] **Basic transform** prediction in advance

- [ ] Streaming with batch transform inference

- [ ] Inference pipeline with multiple containers

- [ ] Customized inference using AWS services

- [ ] Customized inference using containers on AWS services

- [ ] Online inference with online inference pipeline

- [ ] Customized inference hosting using containers with managed services

- [ ] **SageMaker hosted** endpoint

- [ ] SageMaker hosted services

- [x] SageMaker batch transform

- [back to index](root.md)
- [prev  3.1 - Environment](Environment.md)
- [next 3.3 - Deployment Strategies](Deployment_Strategies.md)