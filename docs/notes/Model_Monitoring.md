---
id: 194aakok85wen38zm2qzj1g
title: Model_Monitoring
desc: ''
updated: 1658295655432
created: 1658235655293
---
- [back to index](root.md)
# Workbook 4.2
## Activity – Model Monitoring
![Business Objectives 4.2](media/brain-activity-map.jpg "Model Monitoring")


## 33. How and why might your ML model’s ability to provide accurate prediction decay over time?

| New species of plants and insects are being included in our models. Camera resolutions are changing and vary widely. Seasonal changes in lighting, blooms etc. are not represented in our training data. |
|------------------------------------------------------------------------|



- [back to index](root.md)
- [prev 4.1 - Business Requirements](Busniness_Requirements.md)
- [next 4.3 - Human in the Loop](Human_in_the_Loop.md)
- [next 4.4 - MLOps Monitoring](MLOps_Monitoring.md)    