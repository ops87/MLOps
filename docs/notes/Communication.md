---
id: x0z71r31f2949qrvlmffgac
title: Communication
desc: ''
updated: 1658299622200
created: 1658235344635
---
- [back to index](root.md)
# Workbook 1.1
## 1. Activity – Communication
![Enhancments](media/teamworks.jpg "teamwork with communication")

The purpose of this activity is to strengthen the machine learning
operations (MLOps) communications in your environment.

Fill out the following table. For each of the idealized roles on the
left, write the job title or the name of the person or people in your
organization that are responsible for each kind of work.

## MLOps Team Hierarchy
<details>
  <summary>Click to show diagram</summary>

```mermaid

graph LR;
    BigBoss-->PM;
    PM-->MLOps;
    PM-->DataOps;
    PM-->DataEngineer;
    PM-->DataScientist;
    PM-->DevSecOps;
    PM-->DevOps;
    MLOps-->DataOps;
    DataOps-->DataEngineer;
    DataEngineer-->DataScientist;
    DataScientist-->MLEngineer;
    MLEngineer-->MLOps;
    MLEngineer-->QA
    QA-->SRE;
    MLOps-->QA;
    MLOps-->DevOps;
    MLOps-->SRE;
    DevOps-->MLOps;
    DevOps-->SRE;
    DevSecOps-->DataOps;
    DevSecOps-->MLOps;
    DevSecOps-->DevOps;
    DevSecOps-->MLEngineer;
    DevSecOps-->DataEngineer;
    DevSecOps-->SRE;
```

</details>

### Names in your organization - with key contacts
| Roles                | Business |   Data       |  Develop  |    Deploy   |   Monitor   |
|----------------------|----------|--------------|-----------|-------------|-------------|
| Business stakeholder | Big Boss | Kim Dotcom   | Dave Fun  |  Oppi Zuzu  | Nicky Moley |
| Project manager      |          |              |           |             |             |
| Data engineer        |          |              |           |             |             |
| Data scientist       |          |              |           |             |             |
| ML engineer          |          |              |           |             |             |  
| DevOps engineer      |          |              |           |             |             |
| MLOps engineer       |          |              |           |             |             |
| Software engineer    |          |              |           |             |             |
| Security engineer    |          |              |           |             |             |
| SRE                  |          |              |           |             |             |
| Other                |          |              |           |             |             |

## 2. Do you see any gaps between roles and ML stages? If so, do you think this work is someone’s responsibility, and you might need to investigate further?

| This is a new project so we are sending a team to AWS training to help us identify gaps. We do have a current version in production so we have some success but we could not react fast enough to the MH invasion |
|------------------------------------------------------------------------|

## 3. What opportunities, if any, do you have to improve operations by seeking to assign someone to cover gaps in this table?

| Speed to Market – Manual procedures |
|-------------------------------------|

## 4. Action plan:

| Action / outcome description | Stakeholders                | Considerations    |
|------------------------|------------------------|------------------------|
| AWS Training                 | All                         | Time / Scheduling |
| Process review               | All                         | All               |
| Improved Automation          | AllOps                      |                   |
| Data Scientist Tooling       | ML Engineering/Data Science |                   |
| Improved Security            | AllOps – SecOps Lead        |                   |
|                              |                             |                   |
|                              |                             |                   |

## 5. Questions for the instructor:

| Input something here to ask the instructor                          |
|----------------------------------------------------------------------------------|

- [back to index](root.md)
- [next 2.1 - Challenges](Challenges.md)