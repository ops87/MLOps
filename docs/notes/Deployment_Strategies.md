---
id: 2cp88nznzjearn5b8cpv72t
title: Deployment_Strategies
desc: ''
updated: 1658267815412
created: 1658235492354
---
- [back to index](root.md)


# Workbook 3.3
## 1. Activity – Deployment Strategies

![Workbook 3.2](media/quality-assurance.jpg)

In this workbook activity, you will consider how deployment is currently
implemented in your environment. You will also consider opportunities
for employing MLOps deployment techniques described in this course. In
the final step, you will create an action plan of items to investigate,
discuss, and implement to improve MLOps in your ML workflow.

## 24. Which deployment strategies are relevant for your organization?

- [ ] **Update** (standard update)

- [ ] A/B testing

- [x] Blue/green

- [ ] Canary

- [ ] Champion/challenger

## 25. How often do changes occur to the ML models in production?

| We have a monthly/quarterly release cycle. We need get this down to days and even weeks for some of our solutions |
|------------------------------------------------------------------------|

## 26. What are the key criteria that determine when a new or updated ML model is deployed to production?

| Field testers/Partners report issues. We have some preventative models in place. Changes in mobile device camera resolutions are causing issues. We should be able to monitor app and model performance better |
|------------------------------------------------------------------------|

## 27. Which Amazon SageMaker integration strategies are relevant for your organization?

- [ ] Not currently using any cloud services in our ML process

- [ ] Originate the ML process on the cloud

- [ ] Leverage a built-in algorithm to train models

- [ ] Use a built-in framework for the entire ML process

- [ ] Bring your own model (The model is built on premises and then

- [ ] brought to the cloud for hosting.)

- [ ] Bring your own container

- [ ] Use a hosted ML model (model as a service, such as Amazon Rekognition)

## 28. What methods do you use, or are considering, in your ML process?

|     |
|-----|

## 29. Action plan:

| Action / outcome description | Stakeholders | Considerations |
|------------------------------|--------------|----------------|
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |

## 30. Questions for the instructor:

|     |
|-----|

- [prev 3.1 - Environment](Environment.md)
- [prev 3.2 - Deployment_Solutions](Deployment_Solutions.md)
- [next 4.1 - Business Requirements](Business_Requirements.md)
- [back to index](root.md)
