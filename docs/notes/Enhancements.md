---
id: cwdglaj3gw2ux6sywlzjyck
title: Enhancements
desc: ''
updated: 1658267820424
created: 1658235439869
---
- [back to index](root.md)
## Workbook 2.4
## 15. Activity – Three actions
![Enhancments](media/gokart-2.png)

What are three action items you can take to build/enhance your ML
process?

| Better Communication/Integrated Data Science development/Leverage Cloud Technology |
|------------------------------------------------------------------------|

## 16. Action plan:

| Action / outcome description | Stakeholders | Considerations |
|------------------------------|--------------|----------------|
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |

## 17. Questions for the instructor:

|     |
|-----|
- [prev challenge 2.3 - Frameworks and Tooling](Workflow.md)
- [prev Challenges 2.2 - DevOps to MLOps](DevOps_to_MLOps.md)
- [prev Challenges 2.1 - Challenges](Challenges.md)
- [next Environment 3.1](Environment.md)
- [back to index](root.md)
