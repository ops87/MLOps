---
id: 118dzy9996xwnyfif1bxc6i
title: Workbook Main Page
desc: 'Machine Learning Operations Workbook'
updated: 1658298348875
created: 1658234032344
---

![MLOps Engineering on AWS](media/Title1.png)

### This is the root of your MLOps workbook.
#### Access to all Sections and Diagrams are linked on this main page.

## MLOps stack template
<details>
  <summary>Click to show diagram</summary>

```mermaid

graph TD;
    Data_analysis-->Experimentation;
    Data_analysis-->Feature_store;
    Feature_store-->ML_pipeline;
    Feature_store-->Model_serving;
    Experimentation-->Code_repository;
    Code_repository-->ML_PIPELINE;
    ML_PIPELINE-->Model_registry;
    Model_registry-->Model_serving;
    ML_PIPELINE-->Metadata_store;
    Model_serving-->Model_monitoring;
    Model_monitoring-->ML_PIPELINE;
```

</details>

## MLOps process
<details>
  <summary>Click to show diagram</summary>

```mermaid

graph LR;
    Business_Opportunity-->Problem_Definition;
    Problem_Definition-->DataOps;
    DataOps-->Explore;
    Explore-->Feature-Engineering;
    Feature-Engineering-->Modelling;
    Modelling-->Continuous_Integration;
    Continuous_Integration-->Testing;
    Testing-->Continuous_Delivery;
    Continuous_Delivery-->Monitor_&_Validate;
    Continuous_Delivery-->Approval_Gate;
    Approval_Gate-->Model_Deploy;
    Model_Deploy-->Monitor_&_Validate;
    Monitor_&_Validate-->DataOps;
    Monitor_&_Validate-->Feature-Engineering;
    Monitor_&_Validate-->Modelling;
    Monitor_&_Validate-->Continuous_Integration;
    DataOps-->Problem_Definition;
    Continuous_Delivery-->Problem_Definition;
  
```

</details>

## MLOps project
<details>
  <summary>Click to show diagram</summary>

```mermaid
gantt
    title MLops Pilot Product Roadmap
    dateFormat  YYYY-MM-DD
    section Cool Pipeline
    A task           :a1, 2022-08-01, 30d
    Another task     :after a1  , 20d
    section Another Pipeline
    Task in sec      :2022-08-12  , 12d
    yet another pipeline      : 24d
```
</details>

## Sections
<details>
  <summary>Section 1 Communication</summary>

- [Workbook 1.1 - Communication](Communication.md)
  
</details>

<details>
  <summary>Section 2 Challenges</summary>

- [Workbook 2.1 - Challenges](Challenges.md)
- [Workbook 2.2 - DevOps to MLOps](DevOps_to_MLOps.md)
- [Workbook 2.3 - Workflow](Workflow.md)
- [Workbook 2.3 - Enhancements](Enhancements.md)
  
</details>

<details>
  <summary>Section 3 Environment</summary>

- [Workbook 3.1 - Environment](Environment.md)
- [Workbook 3.2 - Deployment_Solutions](Deployment_Solutions.md)
- [Workbook 3.3 - Deployment Strategies](Deployment_Strategies.md)

  
</details>

<details>
  <summary>Section 4 Business Objectives</summary>

- [Workbook 4.1 - Business Requirements](Business_Requirements.md)
- [Workbook 4.2 - Model Monitoring](Model_Monitoring.md)
- [Workbook 4.3 - Human in the Loop](Human_in_the_Loop.md)
- [Workbook 4.4 - MLOps Monitoring](MLOps_Monitoring.md)    

</details>

<details>
  <summary>Latest Revisions</summary>

- [X] Section 1 Commmunication - last update on 19/07/2022 - [approved by aws](http://www.example.com/documents/approvals/workbook_approvals)
- [ ] Section 2 Challenges - last updated on 19/07/2022
- [ ] Section 3 Environment - last updated on 19/07/2022 
- [ ] Section 4 Business Requirements - last updated on 19/07/2022 

</details>

### Dendron Help
This section below contains useful links to related resources.

- [Getting Started Guide](https://link.dendron.so/6b25)
- [Discord](https://link.dendron.so/6b23)
- [Home Page](https://wiki.dendron.so/)
- [Github](https://link.dendron.so/6b24)
- [Developer Docs](https://docs.dendron.so/)
