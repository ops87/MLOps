---
id: hmwicxcn5ck81t20w1uzpx8
title: MLOps_Monitoring
desc: ''
updated: 1658267812075
created: 1658235692937
---

![Workbook 4.4](media/arrow.png)

## 35. Activity – What is your MLOps monitoring recommendation?

Fill out the following table to list what to monitor, how you’ll monitor
it, and who’s responsible for each task. Note ideas for alerts and
automation.

<table style="width:100%;">
<colgroup>
<col style="width: 18%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 16%" />
</colgroup>
<thead>
<tr class="header">
<th></th>
<th>What to monitor</th>
<th>How to monitor</th>
<th>Who’s responsible</th>
<th>Type(s) of alerts</th>
<th>Automation recommendations</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Business requirements</td>
<td><p>Accuracy/</p>
<p>Drift</p>
<p>Security</p></td>
<td><p>Earlier drift detection</p>
<p>Simulation</p>
<p>Live Performance/</p>
<p>Daily Ground Truth Evaluations</p></td>
<td><p>Product Owners for KPIs</p>
<p>MLOps for Implementation</p></td>
<td><p>Drift</p>
<p>Performance</p>
<p>Stability</p></td>
<td><p>Earlier drift detection</p>
<p>Simulation</p>
<p>Live Performance/</p>
<p>Daily Ground Truth Evaluations</p>
<p>Security Analysis</p></td>
</tr>
<tr class="even">
<td>Model performance</td>
<td>Accuracy (CV Apps)</td>
<td><p>End-to-end Monitoring</p>
<p>Compare against Ground Truth</p></td>
<td><p>Data Science/</p>
<p>MLOps Engineering</p></td>
<td><p>Drift</p>
<p>Training and Evaluation</p>
<p>Post deployment (A/B)</p></td>
<td><p>Model Monitoring</p>
<p>Model Evaluation/Experiments</p></td>
</tr>
<tr class="odd">
<td>Hosting activities</td>
<td><p>Time to deliver predictions</p>
<p>Stability</p>
<p>Security</p></td>
<td><p>Infrastructure Monitoring</p>
<p>Resource Utilization Monitoring</p></td>
<td><p>Developers</p>
<p>DevOps</p>
<p>MLOps</p>
<p>SecOps</p>
<p>DataOps</p></td>
<td><p>Crash detection</p>
<p>Resource Usage</p>
<p>Throughput</p></td>
<td><p>Mobile MDM</p>
<p>CloudWatch</p>
<p>Edge Management</p>
<p>CloudTrail</p>
<p>AWS Security best practices</p></td>
</tr>
</tbody>
</table>

## 36. Action plan:

| Action / outcome description | Stakeholders | Considerations |
|------------------------------|--------------|----------------|
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |

## 37. Questions for the instructor:

|     |
|-----|

- [back to index](root.md)
- [prev 4.1 - Business Requirements](Business_Requirements.md)
- [prev 4.2 - Model Monitoring](Model_Monitoring.md)  
- [prev 4.3 - Human in the Loop](Human_in_the_Loop.md) 

![Workbook EOF](media/arrow.png)