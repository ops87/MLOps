---
id: d96ym2hlok40oyjs8hqaybk
title: Human_in_the_Loop
desc: ''
updated: 1658265314515
created: 1658235669826
---
- [back to index](root.md)


# Workbook 4.3
## Activity – Human in the Loop
![Business Objectives 4.3](media/terminator-2-judgement-day.jpg "Human in the Loop")


## 34. What roles need to work together to integrate human-in-the-loop reviews of model results?

Refer back to Workbook 1.1 to see which roles need to be involved in
monitoring. Are there additional roles that need to be included to
integrate human-in-the-loop reviews?

| AllOps We have some human in the loop processes in place but we need to determine whether this should be for every deployment phase/type of whether we only need approval for certain ranges of KPI variance |
|------------------------------------------------------------------------|

- [back to index](root.md)
- [prev 4.1 - Business Requirements](Business_Requirements.md)
- [prev 4.2 - Model Monitoring](Model_Monitoring.md)  
- [next 4.4 - MLOps Monitoring](MLOps_Monitoring.md) 