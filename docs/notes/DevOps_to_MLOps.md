---
id: mkd63ll6wu91shg8kdh45b6
title: DevOps_to_MLOps
desc: ''
updated: 1658269221156
created: 1658235405025
---
- [back to index](root.md)
# 7. Activity – From DevOps to MLOps
![Alt](media/MLOpsIntersect.png "# Activity – From DevOps to MLOps")



Fill out the following table. The purpose of this activity is to
familiarize yourself with existing conditions in your environment, from
an MLOps perspective. This table will help you verify the tools and
processes that you are using, who is responsible, and potential
opportunities to further improve MLOps in your environment.

<table style="width:100%;">
<colgroup>
<col style="width: 23%" />
<col style="width: 9%" />
<col style="width: 30%" />
<col style="width: 15%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th>Operations tools and processes</th>
<th><p>Do you have this?</p>
<p>[Y/N]</p></th>
<th>Who is responsible for it?</th>
<th>Could it be used better in an MLOps context?</th>
<th>If you do not have it, what are your next steps (action plan)?</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Code building pipeline</td>
<td>Y</td>
<td>DevOps</td>
<td>Y</td>
<td></td>
</tr>
<tr class="even">
<td>Code version control</td>
<td>Y</td>
<td>DevOps/SecOps</td>
<td>Y</td>
<td>Better triggers when data changes</td>
</tr>
<tr class="odd">
<td>Model version control</td>
<td>N</td>
<td>MLOps/DevOps</td>
<td>Y</td>
<td>Development is siloed… models are not tracked well</td>
</tr>
<tr class="even">
<td>Data version control</td>
<td>Y/N</td>
<td>DataOps</td>
<td>Y</td>
<td>DataOps initiative project in parallel</td>
</tr>
<tr class="odd">
<td>Model building pipeline</td>
<td>N</td>
<td>MLOps</td>
<td>Y</td>
<td>Hope to leverage current tech</td>
</tr>
<tr class="even">
<td>Data pipeline</td>
<td>Y/N</td>
<td></td>
<td></td>
<td>DataOps initiative project in parallel</td>
</tr>
<tr class="odd">
<td>Approval process</td>
<td>Y</td>
<td><p>DevOps/SecOps/MLOps</p>
<p>/BusOps</p></td>
<td>Y</td>
<td>Cumbersome w/out value</td>
</tr>
<tr class="even">
<td>Key performance indicators</td>
<td>Y/N</td>
<td><p>DevOps/SecOps/MLOps</p>
<p>/BusOps</p></td>
<td>Y</td>
<td>Focused effort / AWS Training</td>
</tr>
<tr class="odd">
<td>Baseline metrics</td>
<td>Y</td>
<td><p>DevOps/SecOps/MLOps</p>
<p>/BusOps</p></td>
<td></td>
<td>Based on current app</td>
</tr>
</tbody>
</table>

##  [section]

## 8. Who approves models to be deployed to production in your ML environment?

| It is a committee with inconsistent representation and conflicting priorities. There is a lot that could be automated with better KPI’s/Metrics. Business / Data Science / AllOps teams have different terminology. |
|------------------------------------------------------------------------|

## 9. Action plan:

| Action / outcome description | Stakeholders | Considerations |
|------------------------------|--------------|----------------|
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |
|                              |              |                |

## 10. Questions for the instructor:

|     |
|-----|

### Addessing level of MLOps adoption

- MLOps **level 0** suggests that the company uses machine learning and even has in-house data scientists capable of building and deploying models. But ML workflow is entirely manual. This level may suffice “non-tech companies like banks or insurance agencies, who upgrade their models, say, once a year or when another financial crisis occurs,”— Alexander Konduforov clarifies.

- MLOps **level 1** introduces a pipeline for continuous training. Data and models are automatically validated, and retraining is triggered each time when the model performance degrades or fresh data is in place. This scenario may be helpful for solutions that operate in a constantly changing environment and need to proactively address shifts in customer behavior, price rates, and other indicators.

- MLOps **level 2** is achieved when a CI/CD pipeline automates deployment of ML models and components of ML training pipelines in production. This level suits tech-driven companies who have to retrain their models daily if not hourly, update them in minutes and redeploy on thousands of servers simultaneously. Without end-to-end MLOps cycle, such organizations just won’t survive.

- [prev challenge 2.1 - Challenges](Challenges.md)
- [next challenge 2.3 - Frameworks and Tooling](Workflow.md)
- [next challenge 2.4 - Enhancements](Enhancements.md)
- [back to index](root.md)